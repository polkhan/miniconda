# miniconda

Install miniconda to manage Python and data science environments

## Role Variables

* `home`
    * Type: String
    * Usage: Set miniconda home directory
    * Default: `"{{ ansible_env.HOME }}/.config/miniconda"`

* `installer`
    * Type: String
    * Usage: URL to installer download
    * Default: `"https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh"`

* `install_zsh`
    * Type: Boolean
    * Usage: If true, install zsh alias file
    * Default: False

* `zsh_shell_script_path`
    * Type: String
    * Usage: Path name of zsh alias file
    * Default: `"{{ ansible_env.HOME }}/.zshrc.d/miniconda.zsh"`

* `install_fish`
    * Type: Boolean
    * Usage: If true, install fish alias file
    * Default: False

* `fish_shell_script_path`
    * Type: String
    * Usage: Path name of fish shell alias file
    * Default: `"{{ ansible_env.HOME }}/.config/fish/fish.d/miniconda.fish"`

* `fish_right_prompt`
    * Type: Boolean
    * Usage: If false, disable miniconda's right prompt in fish shell
    * Default: False

* `fish_path_reorder`
    * Type: Boolean
    * Usage: If true, installs a hack for nested shells to put custom paths at the beginning of the path
    * Default: False

```yaml
miniconda:
  home: .config/miniconda
  installer: https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh

  install_zsh: True
  install_fish: False
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

```yaml
- hosts: servers
  roles:
   - polka.miniconda
```

## License

MIT
